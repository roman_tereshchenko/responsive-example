import { Component, OnInit } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isMobSafari = false;
  title = 'responsive-example';
  menuToggle = false;
  detailsToggle = false;

  constructor(private ds: DeviceDetectorService) {}

  ngOnInit(): void {
    const device = this.ds.getDeviceInfo();

    const os = device.os;
    const browser = device.browser;

    console.log(os, browser);

    this.checkDeviceInfo(os, browser);
  }

  toggleMenu() {
    this.menuToggle = !this.menuToggle;
  }

  goToDetails(bool): void {
    this.detailsToggle = bool;
  }

  checkDeviceInfo(os, browser) {
    this.isMobSafari = (os === 'iOS' && browser === 'Safari' );
  }
}
